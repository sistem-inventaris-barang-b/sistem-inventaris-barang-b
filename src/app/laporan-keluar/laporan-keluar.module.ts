import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LaporanKeluarPageRoutingModule } from './laporan-keluar-routing.module';

import { LaporanKeluarPage } from './laporan-keluar.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LaporanKeluarPageRoutingModule
  ],
  declarations: [LaporanKeluarPage]
})
export class LaporanKeluarPageModule {}
