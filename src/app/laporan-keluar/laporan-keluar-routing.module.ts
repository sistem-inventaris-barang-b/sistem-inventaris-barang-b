import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LaporanKeluarPage } from './laporan-keluar.page';

const routes: Routes = [
  {
    path: '',
    component: LaporanKeluarPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LaporanKeluarPageRoutingModule {}
