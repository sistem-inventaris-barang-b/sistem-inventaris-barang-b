import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LaporanKeluarPage } from './laporan-keluar.page';

describe('LaporanKeluarPage', () => {
  let component: LaporanKeluarPage;
  let fixture: ComponentFixture<LaporanKeluarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanKeluarPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LaporanKeluarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
