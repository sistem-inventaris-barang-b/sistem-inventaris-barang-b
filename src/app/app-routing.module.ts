import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'sign-in',
    loadChildren: () => import('./sign-in/sign-in.module').then( m => m.SignInPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'sign-up',
    loadChildren: () => import('./sign-up/sign-up.module').then( m => m.SignUpPageModule)
  },
  {
    path: 'menu',
    loadChildren: () => import('./menu/menu.module').then( m => m.MenuPageModule)
  },
  {
    path: 'barang-masuk',
    loadChildren: () => import('./barang-masuk/barang-masuk.module').then( m => m.BarangMasukPageModule)
  },
  {
    path: 'barang-keluar',
    loadChildren: () => import('./barang-keluar/barang-keluar.module').then( m => m.BarangKeluarPageModule)
  },
  {
    path: 'peminjaman',
    loadChildren: () => import('./peminjaman/peminjaman.module').then( m => m.PeminjamanPageModule)
  },
  {
    path: 'penyimpanan',
    loadChildren: () => import('./penyimpanan/penyimpanan.module').then( m => m.PenyimpananPageModule)
  },
  {
    path: 'daftar-user',
    loadChildren: () => import('./daftar-user/daftar-user.module').then( m => m.DaftarUserPageModule)
  },
  {
    path: 'laporan',
    loadChildren: () => import('./laporan/laporan.module').then( m => m.LaporanPageModule)
  },
  {
    path: 'tambah-barang',
    loadChildren: () => import('./tambah-barang/tambah-barang.module').then( m => m.TambahBarangPageModule)
  },
  {
    path: 'tambah-barang-masuk',
    loadChildren: () => import('./tambah-barang-masuk/tambah-barang-masuk.module').then( m => m.TambahBarangMasukPageModule)
  },
  {
    path: 'tambah-barang-keluar',
    loadChildren: () => import('./tambah-barang-keluar/tambah-barang-keluar.module').then( m => m.TambahBarangKeluarPageModule)
  },
  {
    path: 'tambah-barang-id',
    loadChildren: () => import('./tambah-barang-id/tambah-barang-id.module').then( m => m.TambahBarangIdPageModule)
  },
  {
    path: 'penyimpanan-data',
    loadChildren: () => import('./penyimpanan-data/penyimpanan-data.module').then( m => m.PenyimpananDataPageModule)
  },
  {
    path: 'laporan-masuk',
    loadChildren: () => import('./laporan-masuk/laporan-masuk.module').then( m => m.LaporanMasukPageModule)
  },
  {
    path: 'laporan-keluar',
    loadChildren: () => import('./laporan-keluar/laporan-keluar.module').then( m => m.LaporanKeluarPageModule)
  },
  {
    path: 'laporan-stok',
    loadChildren: () => import('./laporan-stok/laporan-stok.module').then( m => m.LaporanStokPageModule)
  },
  {
    path: 'dashboard-admin',
    loadChildren: () => import('./admin/dashboard-admin/dashboard-admin.module').then( m => m.DashboardAdminPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
