import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PenyimpananDataPage } from './penyimpanan-data.page';

const routes: Routes = [
  {
    path: '',
    component: PenyimpananDataPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PenyimpananDataPageRoutingModule {}
