import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PenyimpananDataPageRoutingModule } from './penyimpanan-data-routing.module';

import { PenyimpananDataPage } from './penyimpanan-data.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PenyimpananDataPageRoutingModule
  ],
  declarations: [PenyimpananDataPage]
})
export class PenyimpananDataPageModule {}
