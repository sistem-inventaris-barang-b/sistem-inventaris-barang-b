import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PenyimpananDataPage } from './penyimpanan-data.page';

describe('PenyimpananDataPage', () => {
  let component: PenyimpananDataPage;
  let fixture: ComponentFixture<PenyimpananDataPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PenyimpananDataPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PenyimpananDataPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
