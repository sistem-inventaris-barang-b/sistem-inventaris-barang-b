import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LaporanStokPageRoutingModule } from './laporan-stok-routing.module';

import { LaporanStokPage } from './laporan-stok.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LaporanStokPageRoutingModule
  ],
  declarations: [LaporanStokPage]
})
export class LaporanStokPageModule {}
