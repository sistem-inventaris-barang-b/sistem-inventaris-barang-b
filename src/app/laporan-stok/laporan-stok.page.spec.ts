import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LaporanStokPage } from './laporan-stok.page';

describe('LaporanStokPage', () => {
  let component: LaporanStokPage;
  let fixture: ComponentFixture<LaporanStokPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanStokPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LaporanStokPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
