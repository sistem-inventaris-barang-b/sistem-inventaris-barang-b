import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LaporanStokPage } from './laporan-stok.page';

const routes: Routes = [
  {
    path: '',
    component: LaporanStokPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LaporanStokPageRoutingModule {}
