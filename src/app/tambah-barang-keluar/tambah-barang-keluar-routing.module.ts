import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TambahBarangKeluarPage } from './tambah-barang-keluar.page';

const routes: Routes = [
  {
    path: '',
    component: TambahBarangKeluarPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TambahBarangKeluarPageRoutingModule {}
