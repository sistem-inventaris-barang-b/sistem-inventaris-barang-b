import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TambahBarangKeluarPageRoutingModule } from './tambah-barang-keluar-routing.module';

import { TambahBarangKeluarPage } from './tambah-barang-keluar.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TambahBarangKeluarPageRoutingModule
  ],
  declarations: [TambahBarangKeluarPage]
})
export class TambahBarangKeluarPageModule {}
