import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TambahBarangKeluarPage } from './tambah-barang-keluar.page';

describe('TambahBarangKeluarPage', () => {
  let component: TambahBarangKeluarPage;
  let fixture: ComponentFixture<TambahBarangKeluarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TambahBarangKeluarPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TambahBarangKeluarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
