import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LaporanMasukPage } from './laporan-masuk.page';

const routes: Routes = [
  {
    path: '',
    component: LaporanMasukPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LaporanMasukPageRoutingModule {}
