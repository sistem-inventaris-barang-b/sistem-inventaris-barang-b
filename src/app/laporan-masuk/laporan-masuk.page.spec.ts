import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LaporanMasukPage } from './laporan-masuk.page';

describe('LaporanMasukPage', () => {
  let component: LaporanMasukPage;
  let fixture: ComponentFixture<LaporanMasukPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanMasukPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LaporanMasukPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
