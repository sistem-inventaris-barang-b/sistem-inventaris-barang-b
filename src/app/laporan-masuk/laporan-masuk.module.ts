import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LaporanMasukPageRoutingModule } from './laporan-masuk-routing.module';

import { LaporanMasukPage } from './laporan-masuk.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LaporanMasukPageRoutingModule
  ],
  declarations: [LaporanMasukPage]
})
export class LaporanMasukPageModule {}
