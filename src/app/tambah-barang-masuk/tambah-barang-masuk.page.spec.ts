import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TambahBarangMasukPage } from './tambah-barang-masuk.page';

describe('TambahBarangMasukPage', () => {
  let component: TambahBarangMasukPage;
  let fixture: ComponentFixture<TambahBarangMasukPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TambahBarangMasukPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TambahBarangMasukPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
