import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TambahBarangMasukPage } from './tambah-barang-masuk.page';

const routes: Routes = [
  {
    path: '',
    component: TambahBarangMasukPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TambahBarangMasukPageRoutingModule {}
