import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TambahBarangMasukPageRoutingModule } from './tambah-barang-masuk-routing.module';

import { TambahBarangMasukPage } from './tambah-barang-masuk.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TambahBarangMasukPageRoutingModule
  ],
  declarations: [TambahBarangMasukPage]
})
export class TambahBarangMasukPageModule {}
