import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DaftarUserPage } from './daftar-user.page';

describe('DaftarUserPage', () => {
  let component: DaftarUserPage;
  let fixture: ComponentFixture<DaftarUserPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DaftarUserPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DaftarUserPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
