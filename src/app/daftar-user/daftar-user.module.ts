import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DaftarUserPageRoutingModule } from './daftar-user-routing.module';

import { DaftarUserPage } from './daftar-user.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DaftarUserPageRoutingModule
  ],
  declarations: [DaftarUserPage]
})
export class DaftarUserPageModule {}
