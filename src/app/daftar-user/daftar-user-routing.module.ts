import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DaftarUserPage } from './daftar-user.page';

const routes: Routes = [
  {
    path: '',
    component: DaftarUserPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DaftarUserPageRoutingModule {}
