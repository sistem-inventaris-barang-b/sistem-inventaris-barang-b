import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TambahBarangIdPage } from './tambah-barang-id.page';

describe('TambahBarangIdPage', () => {
  let component: TambahBarangIdPage;
  let fixture: ComponentFixture<TambahBarangIdPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TambahBarangIdPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TambahBarangIdPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
