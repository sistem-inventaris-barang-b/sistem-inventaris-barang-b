import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TambahBarangIdPageRoutingModule } from './tambah-barang-id-routing.module';

import { TambahBarangIdPage } from './tambah-barang-id.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TambahBarangIdPageRoutingModule
  ],
  declarations: [TambahBarangIdPage]
})
export class TambahBarangIdPageModule {}
