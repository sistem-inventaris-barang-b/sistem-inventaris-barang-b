import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TambahBarangIdPage } from './tambah-barang-id.page';

const routes: Routes = [
  {
    path: '',
    component: TambahBarangIdPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TambahBarangIdPageRoutingModule {}
