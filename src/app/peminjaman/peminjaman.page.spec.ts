import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PeminjamanPage } from './peminjaman.page';

describe('PeminjamanPage', () => {
  let component: PeminjamanPage;
  let fixture: ComponentFixture<PeminjamanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeminjamanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PeminjamanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
