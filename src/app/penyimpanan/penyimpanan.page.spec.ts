import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PenyimpananPage } from './penyimpanan.page';

describe('PenyimpananPage', () => {
  let component: PenyimpananPage;
  let fixture: ComponentFixture<PenyimpananPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PenyimpananPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PenyimpananPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
