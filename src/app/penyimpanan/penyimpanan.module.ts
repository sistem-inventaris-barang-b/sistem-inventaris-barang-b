import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PenyimpananPageRoutingModule } from './penyimpanan-routing.module';

import { PenyimpananPage } from './penyimpanan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PenyimpananPageRoutingModule
  ],
  declarations: [PenyimpananPage]
})
export class PenyimpananPageModule {}
