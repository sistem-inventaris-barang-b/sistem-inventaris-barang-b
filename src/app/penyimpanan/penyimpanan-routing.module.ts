import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PenyimpananPage } from './penyimpanan.page';

const routes: Routes = [
  {
    path: '',
    component: PenyimpananPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PenyimpananPageRoutingModule {}
